#include "Shape.h"
#include "GameBoard.h"

Shape::Shape(Point p, int shapeArrX, int shapeArrY) : sizeX(shapeArrX), sizeY(shapeArrY), shape_arr_xy(0)
{
	this->coords = p;
	initShapeArr();
}

Shape::Shape(Point p) :sizeX(0), sizeY(0), shape_arr_xy(0)
{
	this->coords = p;
}

void Shape::initShapeArr()
{
	shape_arr_xy = new char*[sizeX];
	for (int i = 0; i < sizeX; i++)
	{
		shape_arr_xy[i] = new char[sizeY];
	}
	for (int x = 0; x < sizeX; x++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			shape_arr_xy[x][y] = EMPTY_SYMBOL;
		}
	}			
}

int Shape::getSizeX()
{
	return this->sizeX;
}

int Shape::getSizeY()
{
	return this->sizeY;
}

void Shape::clearArray()
{
	for (int i = 0; i < sizeX; i++)
	{
		delete[] shape_arr_xy[i];
	}		
	delete[] shape_arr_xy;
}

void Shape::rotate()
{
	if (shape_arr_xy == 0)
	{
		return;
	}

	char** tmpArr;
	tmpArr = new char*[sizeX];

	for (int x = 0; x < sizeX; x++)
	{
		tmpArr[x] = new char[sizeY];
	}	

	for (int x = 0; x < sizeX; x++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			tmpArr[x][y] = shape_arr_xy[x][y];
		}			
	}

	clearArray();
	int temp = sizeX;
	sizeX = sizeY;
	sizeY = temp;
	initShapeArr();

	for (int x = 0; x < sizeX; x++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			shape_arr_xy[x][y] = tmpArr[y][sizeX - x - 1];
		}			
	}		

	for (int x = 0; x < sizeY; x++)
	{
		delete[] tmpArr[x];
	}		
	delete[] tmpArr;
}

void Shape::flipShape()
{
	int tmp;
	for (int y = 0; y < sizeY; y++)
	{
		for (int x = 0; x < sizeX / 2; x++)
		{
			tmp = shape_arr_xy[x][y];
			shape_arr_xy[x][y] = shape_arr_xy[sizeX - x - 1][y];
			shape_arr_xy[sizeX - x - 1][y] = tmp;
		}
	}		
}

ShapeO::ShapeO(int x, int y) : Shape(Point(x, y), 2, 2)
{
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			shape_arr_xy[i][j] = SHAPE_SYMBOL;
		}
	}		
}

Shape* ShapeO::copy()
{
	ShapeO* pTemp = new ShapeO(this->coords.X, this->coords.Y);
	return pTemp;
}

void ShapeO::rotate() 
{
	//
}

ShapeI::ShapeI(int x, int y, bool sd) : Shape(Point(x, y), 1, 4), shapeSide(0)
{
	for (int x = 0; x < 1; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			shape_arr_xy[x][y] = SHAPE_SYMBOL;
		}
	}
	if (sd != shapeSide)
	{
		rotate();
	}		
}

Shape* ShapeI::copy()
{
	ShapeI* pTemp = new ShapeI(this->coords.X, this->coords.Y, shapeSide);
	return pTemp;
}

void ShapeI::rotate()
{
	Shape::rotate();
	shapeSide = !shapeSide;
}


ShapeT::ShapeT(int x, int y, int sd) : Shape(Point(x, y), 3, 2), shapeSide(0)
{
	shape_arr_xy[1][1] = SHAPE_SYMBOL;
	shape_arr_xy[0][0] = SHAPE_SYMBOL;
	shape_arr_xy[1][0] = SHAPE_SYMBOL;
	shape_arr_xy[2][0] = SHAPE_SYMBOL;

	while (shapeSide < sd)
	{
		rotate();
	}
		
}

Shape * ShapeT::copy()
{
	return new ShapeT(this->coords.X, this->coords.Y, shapeSide);
}

void ShapeT::rotate()
{
	Shape::rotate();
	if (++shapeSide > 3)
	{
		shapeSide = 0;
	}		
}

ShapeL::ShapeL(int x, int y, int sd, bool fp) : Shape(Point(x, y), 2, 3), shapeSide(0), shapeFlip(fp)
{
	shape_arr_xy[0][0] = SHAPE_SYMBOL;
	shape_arr_xy[0][1] = SHAPE_SYMBOL;
	shape_arr_xy[0][2] = SHAPE_SYMBOL;
	shape_arr_xy[1][2] = SHAPE_SYMBOL;

	if (shapeFlip)
	{
		Shape::flipShape();
	}

	while (shapeSide < sd)
	{
		rotate();
	}	
}

Shape * ShapeL::copy()
{
	return new ShapeL(this->coords.X, this->coords.Y, shapeSide, shapeFlip);
}

void ShapeL::rotate()
{
	Shape::rotate();
	if (++shapeSide > 3)
	{
		shapeSide = 0;
	}		
}

ShapeZ::ShapeZ(int x, int y, int sd, bool fp) : Shape(Point(x, y), 3, 2), shapeSide(0), shapeFlip(fp)
{
	shape_arr_xy[0][0] = SHAPE_SYMBOL;
	shape_arr_xy[1][0] = SHAPE_SYMBOL;
	shape_arr_xy[1][1] = SHAPE_SYMBOL;
	shape_arr_xy[2][1] = SHAPE_SYMBOL;

	if (shapeFlip)
	{
		Shape::flipShape();
	}		

	while (shapeSide < sd)
	{
		rotate();
	}		
}

Shape * ShapeZ::copy()
{
	return new ShapeZ(this->coords.X, this->coords.Y, shapeSide, shapeFlip);
}

void ShapeZ::rotate()
{
	Shape::rotate();
	if (++shapeSide > 3)
	{
		shapeSide = 0;
	}		
}