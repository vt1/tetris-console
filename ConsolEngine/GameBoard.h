#pragma once

#include "BaseApp.h"
#include "Shape.h"

#define SHAPE_STATIC_STATE '@'
#define EMPTY_SYMBOL ' '
#define GAME_FIELD_X 15
#define GAME_FIELD_Y 25
#define STEP 1
#define FENCE_SYMBOL '#'

class GameBoard : public BaseApp
{
public:
	GameBoard();
	virtual void KeyPressed(int btnCode);
	virtual void UpdateF(float deltaTime);
	bool createShape();
	void drawShape(Shape* pShape);
	void drawFence();	
	void showScore();
	void deletePrevious();
	bool possibleShapeToRotate();
	void fixShape();
	bool isLineCombo();
	void gameOverLayer();
	bool possibleShapeLeft();
	bool isShapeGoOnDown();	
	void drawNextShape();
	bool possibleShapeRight();		

private:
	Shape* pShape;
	Shape* pPreviousShape;
	int score;
	float curTime;
	int start_shape_Y;
	int start_shape_X;	
	int nextShape;
	bool nextFlip;
	float step;	
	bool downArrowPressed;
};