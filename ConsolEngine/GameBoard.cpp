﻿#include "GameBoard.h"
#include <ctime>
#include <iostream>
#include <conio.h>
#include "PerformanceCounter.h"

GameBoard::GameBoard() : BaseApp(GAME_FIELD_X + 7, GAME_FIELD_Y),
								step(STEP), pPreviousShape(0),
									score(0), curTime(0), downArrowPressed(false)
{
	start_shape_X = 6;
	start_shape_Y = 1;
	srand(time(0));
	nextShape = rand() % 5;
	nextFlip = rand() % 2;
	createShape();
}

void GameBoard::KeyPressed(int btnCode)
{
	if (btnCode == 224)
	{
		btnCode = getch();
	}
	
	if (btnCode == 75)
	{
		if (possibleShapeLeft())
		{
			pShape->coords.X--;
		}			
	}
	else if (btnCode == 77) 
	{
		if (possibleShapeRight())
		{
			pShape->coords.X++;
		}			
	}	
	else if (btnCode == 80) 
	{	
		downArrowPressed = true;
		if (isShapeGoOnDown())
		{						
			pShape->coords.Y++;
			curTime = 0;
		}
		else
		{
			curTime = step;			
		}				
	}
	else if (btnCode == 32)
	{
		deletePrevious();
		if (possibleShapeToRotate())
		{
			pShape->rotate();
		}
		pPreviousShape = pShape->copy();		
	}
	if (pShape->coords.X < 0) //right fence
	{
		pShape->coords.X = 0;
	}		
	else if (pShape->coords.X + pShape->getSizeX() > GAME_FIELD_X - 1)
	{
		pShape->coords.X = GAME_FIELD_X - pShape->getSizeX();
	}		
}

void GameBoard::drawFence()
{
	int tmpY = Y_SIZE - 5;
	int tmpX = GAME_FIELD_X + 7;
	for (int x = 0; x <= X_SIZE; x++)
	{
		SetChar(x, 0, FENCE_SYMBOL);
	}

	for (int x = 0; x <= GAME_FIELD_X + 7; x++)
	{
		SetChar(x, tmpY, FENCE_SYMBOL);
	}

	for (int y = 0; y <= Y_SIZE; y++)
	{
		SetChar(0, y, FENCE_SYMBOL);
		if (y < tmpY)
		{
			SetChar(GAME_FIELD_X, y, FENCE_SYMBOL);
		}
	}

	for (int y = 0; y <= Y_SIZE; y++)
	{
		SetChar(X_SIZE, y, FENCE_SYMBOL);
	}

	for (int x = 0; x <= tmpX; x++)
	{
		SetChar(x, GAME_FIELD_Y, FENCE_SYMBOL);
	}
}

void GameBoard::UpdateF(float deltaTime)
{
	deletePrevious();
	isLineCombo();
	curTime += deltaTime;
	if (curTime > step)
	{
		if (downArrowPressed)
			curTime = 0.9;
		else
			curTime = 0;
		
		if (isShapeGoOnDown())
		{
			pShape->coords.Y++;
		}			
		else
		{
			fixShape();
			if (!createShape())
			{
				gameOverLayer();
			}				
		}
	}

	if (!isShapeGoOnDown())
	{
		downArrowPressed = false;
	}

	pPreviousShape = pShape->copy();
	drawShape(pShape);
	drawFence();
	drawNextShape();
	showScore();	
}

void GameBoard::drawShape(Shape* pShape)
{
	for (int x = 0; x < pShape->getSizeX(); x++)
	{
		for (int y = 0; y < pShape->getSizeY(); y++)
		{
			if (pShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				SetChar(x + pShape->coords.X, y + pShape->coords.Y, pShape->shape_arr_xy[x][y]);
			}				
		}
	}			
}

void GameBoard::drawNextShape()
{
	for (int y = 1; y < 5; y++)
	{
		for (int x = X_SIZE - 6; x < X_SIZE; x++)
		{
			SetChar(x, y, EMPTY_SYMBOL);
		}
	}					
	Shape* sp = 0;
	switch (nextShape)
	{
		case 0:
			sp = new ShapeO(X_SIZE - 4, 2);
			break;
		case 1:
			sp = new ShapeI(X_SIZE - 5, 3, true);
			break;
		case 2:
			sp = new ShapeT(X_SIZE - 5, 2, 2);
			break;
		case 3:			
			if (nextFlip)
			{
				sp = new ShapeL(X_SIZE - 5, 2, 1, nextFlip);
			}
			else
			{
				sp = new ShapeL(X_SIZE - 5, 2, 3, nextFlip);
			}
			break;
		case 4:
			sp = new ShapeZ(X_SIZE - 5, 2, 0, nextFlip);
			break;
		default:
			return;
			break;
	}
	drawShape(sp);
}

void GameBoard::showScore()
{
	int tmpY = Y_SIZE - 2;
	string info_score = to_string(score);
	for (int i = 0; i < 6; i++)
	{
		SetChar(3 + i, tmpY, *(L"Score:" + i));
	}		
	for (int i = 0; i < info_score.length(); i++)
	{
		SetChar(10 + i, tmpY, info_score[i]);
	}		
}

void GameBoard::deletePrevious()
{
	for (int x = 0; x < pPreviousShape->getSizeX(); x++)
	{
		for (int y = 0; y < pPreviousShape->getSizeY(); y++)
		{
			if (pPreviousShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				SetChar(x + pPreviousShape->coords.X, y + pPreviousShape->coords.Y, EMPTY_SYMBOL);
			}				
		}
	}		
}

void GameBoard::fixShape()
{
	deletePrevious();
	for (int x = 0; x < pShape->getSizeX(); x++)
	{
		for (int y = 0; y < pShape->getSizeY(); y++)
		{
			if (pShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				SetChar(x + pShape->coords.X, y + pShape->coords.Y, SHAPE_STATIC_STATE);
			}				
		}
	}			
	delete pShape;
	delete pPreviousShape;
}

bool GameBoard::createShape()
{
	int shape = nextShape;
	nextShape = rand() % 5;
	int side = rand() % 4;
	bool flip = nextFlip;
	nextFlip = rand() % 2;
	switch (shape)
	{
	case 0:
		pShape = new ShapeO(start_shape_X, start_shape_Y);
		break;
	case 1:
		if (bool(side > 1))
		{
			side -= 2;
		}
		pShape = new ShapeI(start_shape_X, start_shape_Y, side);
		break;
	case 2:
		pShape = new ShapeT(start_shape_X, start_shape_Y, side);
		break;
	case 3:
		pShape = new ShapeL(start_shape_X, start_shape_Y, side, flip);
		break;
	case 4:
		pShape = new ShapeZ(start_shape_X, start_shape_Y, side, flip);
		break;
	default:
		break;
	}
	for (int x = 0; x < pShape->getSizeX(); x++)
	{
		for (int y = 0; y < pShape->getSizeY(); y++)
		{
			if (GetChar(pShape->coords.X + x, pShape->coords.Y + y) != EMPTY_SYMBOL)
			{
				return false;
			}
		}
	}
	pPreviousShape = pShape->copy();
	return true;
}

bool GameBoard::possibleShapeToRotate()
{
	Shape *tmpShape = pShape->copy();
	tmpShape->rotate();
	for (int y = 0; y < tmpShape->getSizeY(); y++)
	{
		for (int x = 0; x < tmpShape->getSizeX(); x++)
		{
			if (tmpShape->shape_arr_xy[x][y] != EMPTY_SYMBOL &&
				GetChar(tmpShape->coords.X + x, tmpShape->coords.Y + y) == SHAPE_STATIC_STATE)
			{
				return false;
			}
		}
	}
	return true;
}

bool GameBoard::isShapeGoOnDown()
{
	int tmpY = Y_SIZE - 5;
	if (pShape->coords.Y + pShape->getSizeY() > tmpY)
	{
		return false;
	}		
	for (int x = 0; x < pShape->getSizeX(); x++)
	{
		for (int y = pShape->getSizeY() - 1; y >= 0; y--)
		{
			if (pShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				if (GetChar(pShape->coords.X + x, pShape->coords.Y + y + 1) != EMPTY_SYMBOL)
				{
					return false;
				}
				else
				{
					break;
				}					
			}
		}
	}			
	return true;
}

bool GameBoard::possibleShapeRight()
{
	for (int y = 0; y < pShape->getSizeY(); y++)
	{
		for (int x = pShape->getSizeX() - 1; x >= 0; x--)
		{
			if (pShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				if (GetChar(pShape->coords.X + x + 1, pShape->coords.Y + y) == SHAPE_STATIC_STATE)
				{
					return false;
				}					
				else
				{
					break;
				}					
			}
		}
	}		
	return true;
}

void GameBoard::gameOverLayer()
{
	cout << "game over";
	exit(0);
}

bool GameBoard::possibleShapeLeft()
{
	for (int y = 0; y < pShape->getSizeY(); y++)
	{
		for (int x = 0; x < pShape->getSizeX(); x++)
		{
			if (pShape->shape_arr_xy[x][y] != EMPTY_SYMBOL)
			{
				if (GetChar(pShape->coords.X + x - 1, pShape->coords.Y + y) != EMPTY_SYMBOL)
				{
					return false;
				}					
				else
				{
					break;
				}					
			}
		}
	}			
	return true;
}

bool GameBoard::isLineCombo()
{
	bool fullComboLine;
	int tmpY = Y_SIZE - 5;
	for (int y = tmpY - 1; y > 0; y--)
	{
		fullComboLine = true;
		for (int x = 1; x < GAME_FIELD_X; x++)
		{
			if (GetChar(x, y) == EMPTY_SYMBOL)
			{
				fullComboLine = false;
				break;
			}
		}			
		if (!fullComboLine)
		{
			continue;
		}
		for (int x = 1; x < GAME_FIELD_X; x++)
		{
			SetChar(x, y, EMPTY_SYMBOL);
		}
		for (int i = y; i > 1; i--)
		{
			for (int x = 0; x < GAME_FIELD_X; x++)
			{
				SetChar(x, i, GetChar(x, i - 1));
			}				
		}
		score += 100;
		return true;
	}
	return false;
}

