﻿#pragma once

#include "Point.h"
#define SHAPE_SYMBOL '+'

class Shape
{
public:
	Shape(Point p, int shapeArrX, int shapeArrY);
	Shape(Point p);
	void clearArray();
	void initShapeArr();
	void flipShape();
	virtual Shape* copy() = 0;
	int getSizeX();
	int getSizeY();
	virtual void rotate() = 0;	

	Point coords;
	char **shape_arr_xy;	
	int sizeX;
	int sizeY;
};

class ShapeO : public Shape
{
public:
	ShapeO(int x, int y);
	virtual Shape* copy();
	virtual void rotate();
};

class ShapeT : public Shape
{
public:
	ShapeT(int x, int y, int sd);
	virtual Shape* copy();
	virtual void rotate();
	int shapeSide;
};

class ShapeL : public Shape
{
public:
	ShapeL(int x, int y, int sd, bool fp);
	virtual Shape* copy();
	virtual void rotate();
	bool shapeFlip;
	int shapeSide;
};

class ShapeI : public Shape
{
public:
	ShapeI(int x, int y, bool sd);
	virtual Shape* copy();
	virtual void rotate();
	bool shapeSide;
};

class ShapeZ : public Shape
{
public:
	ShapeZ(int x, int y, int sd, bool fp);
	virtual Shape* copy();
	virtual void rotate();
	int shapeSide;
	bool shapeFlip;
};